export function getDevStatus(item){
	if(item.data?.substring(2,3)  == '1') return '报警'
	if(item.data?.substring(3) == '1') return '运行中'
	if(item.data?.substring(1,2) == '1') return '待机'
	if(item.data?.substring(0,1) == '1') return '关机'
	return '关机'
}
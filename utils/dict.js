import request from '@/config/request.js';

export function getDicts(dictType) {
	return request.get('/system/dict/data/type/' + dictType)
}
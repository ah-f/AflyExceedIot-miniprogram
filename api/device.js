import request from '@/config/request.js';

// 获取设备列表
export const getDevs = () => request.get('/business/micro/devList')
// 获取设备折线图数据
export const getDevLineData = (devNo) => request.get('/business/micro/devLine?deviceNo=' + devNo)


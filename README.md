# 企业级物联网项目（wx小程序）

| 项目模块 | 仓库地址 |
|------|------|
| 前端     | https://gitee.com/ah-f/AflyExceedIot-front       |
| 后端     | https://gitee.com/ah-f/AflyExceedIot-backend     |
| 设备网关服务 | https://gitee.com/ah-f/AflyExceedIot-gateway     |
| wx小程序  | https://gitee.com/ah-f/AflyExceedIot-miniprogram |


具体项目介绍看这里👉[项目介绍](https://gitee.com/ah-f/AflyExceedIot-backend)

